﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AplicacionPrueba.Contract
{
    public interface ICategoria
    {
        bool GuardarCategoria(Categoria categoria);

        List<Categoria> ObtenerCategorias();
    }
}
