﻿using AplicacionPrueba.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AplicacionPrueba.Controllers
{
    public class HomeController : Controller
    {
        private readonly ICategoria categoriaService;
        public HomeController(ICategoria categoriaService) {
            this.categoriaService = categoriaService;
        }

        public ActionResult Index()
        {
            var resultado = categoriaService.ObtenerCategorias()
                .Select(x => new Models.CategoriaModel() {
                    Id = x.Id,
                    Titulo = x.Titulo
                }).ToList();

            var objetoUnico = new Categoria();
            var objetoModel = new Models.CategoriaModel()
            {
                Id = resultado.FirstOrDefault().Id,
                Titulo = resultado.FirstOrDefault().Titulo
            };

            return View(resultado);
        }

        [HttpPost]
        public ActionResult Edit(Models.CategoriaModel model)
        {
            var resultado = categoriaService.GuardarCategoria(new Categoria() {
                Id = model.Id,
                Titulo = model.Titulo
            });
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [HttpPost]
        public ActionResult Contact(ContactModel model)
        {
            if (ModelState.IsValid)
            {
                return RedirectToAction("Contact");
            }
            else
            {
                ViewBag.Message = service.GetTestMessage();
                return View(model);
            }
        }
    }
}