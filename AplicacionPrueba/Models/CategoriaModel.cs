﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AplicacionPrueba.Models
{
    public class CategoriaModel
    {
        [Required]
        public int Id { get; set; }

        [Required]
        public string Titulo { get; set; }
    }
}