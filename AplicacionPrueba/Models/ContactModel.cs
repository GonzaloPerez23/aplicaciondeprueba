﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AplicacionPrueba.Models
{
    public class ContactModel
    {
        [Required]
        [Range(1000000, 50000000)]
        public int Dni { get; set; }

        [Required]
        [StringLength(100, MinimumLength = 5, ErrorMessage = "El valor debe trner entre 5 y 100 caracteres")]
        public string Nombre { get; set; }

        [Required]
        [StringLength(100)]
        public string Apellido { get; set; }

        [Required]
        [RegularExpression(@"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z")]
        public string Email { get; set; }

        [Required]
        [StringLength(16, MinimumLength = 6)]
        public string Contraseña { get; set; }

        [Required]
        [Compare("Contraseña")]
        [StringLength(16, MinimumLength = 6)]
        [Display(Name = "Repertir Contraseña")]
        public string RepetirContraseña { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            List<ValidationResult> validation = new List<ValidationResult>();

            if (this.Apellido.Split(' ').Count() > 3)
            {
                validation.Add(new ValidationResult("El apellido no puede contener mas de 3 palabras.", new[] { "Apellido" }));
            }
            if (this.Nombre.Split(' ').Count() > 3)
            {
                validation.Add(new ValidationResult("El nombre no puede contener mas de 3 palabras.", new[] { "Nombre" }));
            }

            return validation;
        }
    }
}